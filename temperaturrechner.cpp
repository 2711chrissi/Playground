// Author: Christoph Gerlach
// Date: 31.03.2022
// License: Unlicense License
// File: temperaturrechner.cpp
// 
//
// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to <http://unlicense.org/>

#include <iostream>
#include <string>

void celsiusToKelvin() {
    float celsius;
    std::cout << "How much degree celsius?: " << std::endl;
    std::cin >> celsius;
    float kelvin = celsius + 273.15;
    std::cout << celsius << " degree celsius are " << kelvin << " degree kelvin." << std::endl;
    std::cout << "<ENTER> to continue" << std::endl;
    std::cin.get();
}

void celsiusToFahrenheit() {
    float celsius;
    std::cout << "How much degree celsius?: " << std::endl;
    std::cin >> celsius;
    float fahrenheit = celsius * 1.8000 + 32.00;
    std::cout << celsius << " degree celsius are " << fahrenheit << " degree fahrenheit." << std::endl;
    std::cout << "<ENTER> to continue" << std::endl;
    std::cin.get();
}

void kelvinToCelsius() {
    float kelvin;
    std::cout << "How much degree kelvin?: " << std::endl;
    std::cin >> kelvin;
    float celsius = kelvin - 273.15;
    std::cout << kelvin << " degree kelvin are " << celsius << " degree celsius." << std::endl;
    std::cout << "<ENTER> to continue" << std::endl;
    std::cin.get();
}

void kelvinToFahrenheit() {
    float kelvin;
    std::cout << "How much degree kelvin?: " << std::endl;
    std::cin >> kelvin;
    float fahrenheit = (kelvin - 273.15) * 1.8000 + 32.00;
    std::cout << kelvin << " degree kelvin are " << fahrenheit << " degree fahrenheit." << std::endl;
    std::cout << "<ENTER> to continue" << std::endl;
    std::cin.get();
}

void fahrenheitToCelsius() {
    float fahrenheit;
    std::cout << "How much degree fahrenheit?: " << std::endl;
    std::cin >> fahrenheit;
    float celsius = (fahrenheit - 32) / 1.8000;
    std::cout << fahrenheit << " degree fahrenheit are " << celsius << " degree celsius." << std::endl;
    std::cout << "<ENTER> to continue" << std::endl;
    std::cin.get();
}

void fahrenheitToKelvin() {
    float fahrenheit;
    std::cout << "How much degree fahrenheit?: " << std::endl;
    std::cin >> fahrenheit;
    float kelvin = (fahrenheit - 32) / 1.8000 + 273.15;
    std::cout << fahrenheit << " degree fahrenheit are " << kelvin << " degree kelvin." << std::endl;
    std::cout << "<ENTER> to continue" << std::endl;
    std::cin.get();
}

int main() {
    bool end = false;
    while(end == false) {
        std::cout << "---Temperature converter---\nPlease choose:" << std::endl;
        std::cout << "(1) Conversion from celsius to kelvin" << std::endl;
        std::cout << "(2) Conversion from celsius to fahrenheit" << std::endl;
        std::cout << "(3) Conversion from kelvin to celsius" << std::endl;
        std::cout << "(4) Conversion from kelvin to fahrenheit" << std::endl;
        std::cout << "(5) Conversion from fahrenheit to celsuis" << std::endl;
        std::cout << "(6) Conversion from fahrenheit to kelvin" << std::endl;
        std::cout << "(7) Quit" << std::endl;
        std::cout << "Your choice: ";
        int input;
        std::cin >> input;
        if(input == 1) {
            celsiusToKelvin();
            std::cout << "<ENTER> to continue" << std::endl;
            std::cin.get();
            continue;
        } else if(input == 2) {
            celsiusToFahrenheit();
            std::cout << "<ENTER> to continue" << std::endl;
            std::cin.get();
            continue;
        } else if(input == 3) {
            kelvinToCelsius();
            std::cout << "<ENTER> to continue" << std::endl;
            std::cin.get();
            continue;
        } else if(input == 4) {
            kelvinToFahrenheit();
            std::cout << "<ENTER> to continue" << std::endl;
            std::cin.get();
            continue;
        } else if(input == 5) {
            fahrenheitToCelsius();
            std::cout << "<ENTER> to continue" << std::endl;
            std::cin.get();
            continue;
        } else if(input == 6) {
            fahrenheitToKelvin();
            std::cout << "<ENTER> to continue" << std::endl;
            std::cin.get();
            continue;
        } else if(input == 7) {
            std::cout << "Bye!" << std::endl;
            end = true;
        } else {
            std::cout << "Input error!" << std::endl;
            std::cout << "<ENTER> to continue" << std::endl;
            std::cin.get();
            continue;
        }
    }
}
