# Author: Christoph Gerlach
# Date: 28.03.2022
# License: Unlicense License
# File: temperaturrechner.py
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

def celsiusToKelvin():
    celsius = float(input("How much degree celsius?: "))
    kelvin = celsius + 273.15
    print(celsius, "degree celsius are", round(kelvin, 2), "degree kelvin.\n" )
    input("<ENTER> to continue")

def celsiusToFahrenheit():
    celsius = float(input("How much degree celsius?: "))
    fahrenheit = celsius * 1.8000 + 32.00
    print(celsius, "degree celsius are", round(fahrenheit, 2), "degree fahrenheit.\n")
    input("<ENTER> to continue")

def kelvinToCelsius():
    kelvin = float(input("How much degree kelvin?: "))
    celsius = kelvin - 273.15
    print(kelvin, "degree kelvin are", round(celsius, 2), "degree celsius.\n")
    input("<ENTER> to continue")

def kelvinToFahrenheit():
    kelvin = float(input("How much degree kelvin?: "))
    fahrenheit = (kelvin - 273.15) * 1.8000 + 32.00
    print(kelvin, "degree kelvin are", round(fahrenheit, 2), "degree fahrenheit.\n")
    input("<ENTER> to continue")

def fahrenheitToCelsius():
    fahrenheit = float(input("How much degree fahrenheit?: "))
    celsius = (fahrenheit - 32) / 1.8000
    print(fahrenheit, "degree fahrenheit are", round(celsius, 2), "degree celsius.\n")
    input("<ENTER> to continue")

def fahrenheitToKelvin():
    fahrenheit = float(input("How much degree fahrenheit?: "))
    kelvin = (fahrenheit - 32) / 1.8000 + 273.15
    print(fahrenheit, "degree fahrenheit are", round(kelvin, 2), "degree kelvin.\n")
    input("<ENTER> to continue")

end = False

while end == False:
    print("---Temperature converter---\nPlease choose:")
    print("(1) Conversion from celsius to kelvin")
    print("(2) Conversion from celsius to fahrenheit")
    print("(3) Conversion from kelvin to celsius")
    print("(4) Conversion from kelvin to fahrenheit")
    print("(5) Conversion from fahrenheit to celsius")
    print("(6) Conversion from fahrenheit to kelvin")
    print("(7) Quit")
    try:
        choice = input("Your choice: ")
        if choice == '1':
            celsiusToKelvin()
            continue

        elif choice == '2':
            celsiusToFahrenheit()
            continue
        
        elif choice == '3':
            kelvinToCelsius()
            continue
        
        elif choice == '4':
            kelvinToFahrenheit()
            continue
        
        elif choice == '5':
            fahrenheitToCelsius()
            continue
        
        elif choice == '6':
            fahrenheitToKelvin()
            continue
        
        elif choice == '7':
            print("Bye!")
            input("<ENTER> to Quit")
            end = True
        else:
            print("Please choose an option (1-7)!")
            input("<ENTER> to continue")
            continue

    except TypeError:
        print("Input error!")
        input("<ENTER> to continue")

    except ValueError:
        print("Plese choose an option (1-7)!")
        input("<ENTER> to continue")
        continue

    except KeyboardInterrupt:
        print("Bye!")
        end = True
