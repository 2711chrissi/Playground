// Author: Christoph Gerlach
// Date: 31.03.2022
// License: Unlicense License
// File: temperaturrechner.c
//
// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to <http://unlicense.org/>

#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

void celsiusToKelvin() {
    float celsius;
    printf("How much degree celsius?: ");
    scanf("%f", &celsius);
    float kelvin = celsius + 273.15;
    printf("%f degree celsius are %f degree kelvin\n", celsius, kelvin);
    sleep(5);
}

void celsiusToFahrenheit() {
    float celsius;
    printf("How much degree celsius?: ");
    scanf("%f", &celsius);
    float fahrenheit = celsius * 1.8000 + 32.00;
    printf("%f degree celsius are %f degree fahrenheit\n", celsius, fahrenheit);
    sleep(5);
}

void kelvinToCelsius() {
    float kelvin;
    printf("How much degree kelvin?: ");
    scanf("%f", &kelvin);
    float celsius = kelvin - 273.15;
    printf("%f degree kelvin are %f degree celsius\n", kelvin, celsius);
    sleep(5);
}

void kelvinToFahrenheit() {
    float kelvin;
    printf("How much degree kelvin?: ");
    scanf("%f", &kelvin);
    float fahrenheit = (kelvin - 273.15) * 1.8000 + 32.00;
    printf("%f degree kelvin are %f degree fahrenheit\n", kelvin, fahrenheit);
    sleep(5);
}

void fahrenheitToCelsius() {
    float fahrenheit;
    printf("How much degree fahrenheit?: ");
    scanf("%f", &fahrenheit);
    float celsius = (fahrenheit - 32) / 1.8000;
    printf("%f degree fahrenheit are %f degree Celsius\n", fahrenheit, celsius);
    sleep(5);
}

void fahrenheitToKelvin() {
    float fahrenheit;
    printf("How much degree fahrenheit?: ");
    scanf("%f", &fahrenheit);
    float kelvin = (fahrenheit - 32) / 1.8000 + 273.15;
    printf("%f degree fahrenheit are %f degree kelvin\n", fahrenheit, kelvin);
    sleep(5);
}

int main() {
    bool end = false;
    while(end == false) {
        printf("---Temperature converter---\nPlease choose:\n");
        printf("(1) Conversion from celsius to kelvin\n");
        printf("(2) Conversion from celsius to fahrenheit\n");
        printf("(3) Conversion from kelvin to celsius\n");
        printf("(4) Conversion from kelvin to fahrenheit\n");
        printf("(5) Conversion from fahrenheit to celsius\n");
        printf("(6) Conversion from fahrenheit to kelvin\n");
        printf("(7) Quit\n");
        printf("Your choice: ");
        int input;
        scanf("%i", &input);
        if(input == 1) {
            celsiusToKelvin();
            continue;
        } else if(input == 2) {
            celsiusToFahrenheit();
            continue;
        } else if(input == 3) {
            kelvinToCelsius();
            continue;
        } else if(input == 4) {
            kelvinToFahrenheit();
            continue;
        } else if(input == 5) {
            fahrenheitToCelsius();
            continue;
        } else if(input == 6) {
            fahrenheitToKelvin();
            continue;
        } else if(input == 7) {
            printf("Bye!\n");
            sleep(5);
            return 0;
        } else {
            printf("Input error!\n");
            sleep(5);
            continue;
        }
    }
}
